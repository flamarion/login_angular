application.controller("loginCtrl", function ($scope, $http, UserService, AuthService, $window) {

	$scope.users = Array();

	$scope.autenticar = function (user) {
		AuthService.login(user).
			success(function (data, status) {
				AuthService.setToken(data.token);
				alert(AuthService.getToken());
			}).
			error(function (data, status) {
				console.log(data);
				alert("Não foi possível autenticar!");
			});
	};

	$scope.ver_token = function () {
		if($window.localStorage.getItem('token')) {
			$scope.token_atual = $window.localStorage.getItem('token');
		}
		else{
			alert("Não há Token! Login disponível!");
		}
	}

	$scope.verificar_token = function () {
		AuthService.verifyToken().
			success(function (data, status) {
				console.log("Token válido!");
			}).
			error(function (data, status) {
				console.log(data);
			});
	}

	$scope.sair = function () {
		if($window.localStorage.getItem('token')) {
			delete $window.localStorage.removeItem('token');
			alert("Saiu!");
		}
	}


	$scope.cadastrar = function (new_user) {
		UserService.addUser(new_user).
			success(function (status, data, config, headers) {
				alert("Cadastrado com sucesso!");
			}).
			error(function (status, data, config, headers) {
				alert("Não foi possível cadastrar o usuário!");
			});
	}

	var carregarUsuarios = function () {
		UserService.getUsers().
			success(function (data, status, config, headers) {
				$scope.usuarios = data;
				console.log("Usuarios carregados!");
			}).
			error(function (data, status, config, headers) {
				alert("Não foi possível carregar os usuários!");
			});
	};

	carregarUsuarios();

});