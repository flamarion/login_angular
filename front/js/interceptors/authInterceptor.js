application.factory('AuthInterceptor', AuthInterceptor);

application.config(function ($httpProvider) {
	$httpProvider.interceptors.push('AuthInterceptor');
});

function AuthInterceptor ($injector, $window, $q) {
	return {
		request: function (config) {
			console.log("Fazendo requisição!");
			config.headers = config.headers || {};

			var AuthService = $injector.get('AuthService');

			if (AuthService.getToken()) {
				config.headers['Authorization'] = 'Bearer ' + AuthService.getToken();
				console.log("Token adicionado ao header!");
			}
			else{
				console.log("Sem token...");
				window.location.href = 'http://fabricadesoftware.ifc.edu.br/';
			}

			return config;
		},
		responseError: function (response) {
			console.log("Response com erro!");
			if (response.status === 401 || response.status === 403) {
				alert("Faça o Login!");
			}

			return $q.reject(response);
		}
	}
}