application.factory('AuthInterceptor', AuthInterceptor);

application.config(function ($httpProvider) {
	$httpProvider.interceptors.push('AuthInterceptor');
});

function AuthInterceptor ($q) {
	return {
		request: function (data) {
			//console.log("Fazendo requisição!");
			return data;
		},
		requestError: function (rejection) {
			//console.log("Request com erro!"+rejection);
			return $q.reject(rejection);
		},
		response: function (response) {
			//console.log("Obtendo resposta: "+response);
			return response;
		},
		responseError: function (rejection) {
			//console.log("Response com erro!"+rejection);
			return $q.reject(rejection);
		}
	}
}