application.factory('AuthInterceptor', AuthInterceptor);

application.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.interceptors.push(['$location', '$injector', '$q', AuthInterceptor]);
}]);

function AuthInterceptor ($location, $injector, $q) {
	return {
		request: function(config) {
			config.headers = config.headers || {};

			var AuthService = $injector.get('AuthService');

			if (AuthService.getToken()) {
				config.headers['Authorization'] = 'Bearer ' + AuthService.getToken();
			}

			return config;
		},

		responseError: function(response) {
			if (response.status === 401 || response.status === 403) {
				alert("Unauthorized!");
			}

			return $q.reject(response);
		}
	}
}