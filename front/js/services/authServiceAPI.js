application.factory('AuthService', function ($http, $window) {
	return {
		login: function (user) {
			return $http.post(URL_OBTAIN_TOKEN, user);
		},
		verifyToken: function () {
			var token = $window.localStorage.getItem('token');
			return $http.post(URL_VERIFY_TOKEN, {token: token});
		},
		setToken: function (token) {
			$window.localStorage.setItem('token', token);
		},
		getToken: function () {
			return $window.localStorage.getItem('token');
		},
		logout: function () {
			$window.localStorage.removeItem('token');
		}
	};
})