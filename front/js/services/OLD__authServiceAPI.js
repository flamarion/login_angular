application.factory('AuthService', AuthService);

function AuthService ($http, $localStorage, $q) {
    return {
        getToken : function () {
            return $localStorage.token;
        },
        setToken: function (token) {
            $localStorage.token = token;
        },
        signin : function (data) {
            $http.post(URL_LOGIN, data);
        },
        logout : function (data) {
            delete $localStorage.token;
            $q.when();
        }
    };
}