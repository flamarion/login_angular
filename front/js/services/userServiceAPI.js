application.factory("UserService", function ($http) {

	return {
		getUsers: function () {
			return $http.get(URL_USER);
		},
		addUser: function (user) {
			return $http.post(URL_USER, user);
		}
	};
})