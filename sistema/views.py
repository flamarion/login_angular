from django.shortcuts import render, HttpResponseRedirect
from django.views.generic import View
from django.core.urlresolvers import reverse
from sistema.forms import CadUsuarioForm
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.models import User
from sistema.serializers import UserSerializer
from rest_framework import mixins, generics, permissions


# Create your views here.

class Index(View):

    def get(self, request):
        return render(request, 'index.html')

class Login(View):

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
            else:
                print("Usuario %s esta inativo" %username)
        else:
            print("Usuario ou Senha incorretos")
        return HttpResponseRedirect(reverse('index'))

class Logout(View):

    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('index'))

class CadUsuario(View):

    def get(self, request):
        form = CadUsuarioForm()
        return render(request, 'cadastro.html', {
            'form': form
        })

    def post(self, request):
        form = CadUsuarioForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            print('Cliente cadastrado com sucesso!')
            return HttpResponseRedirect(reverse('cadastro'))

        return render(request, 'cadastro.html', {
            'form': form
        })

class UserList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):

    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [
        permissions.AllowAny
    ]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        print(request)
        return self.create(request, *args, **kwargs)
