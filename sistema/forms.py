#coding:utf-8
from django import forms
from django.contrib.auth.models import User

class CadUsuarioForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'username', 'email', 'password']